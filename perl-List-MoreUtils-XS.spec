Name :         perl-List-MoreUtils-XS
Version:       0.430
Release:       3
Summary:       Provide compiled List::MoreUtils functions
License:       (GPL-1.0-or-later OR Artistic-1.0-Perl) and Apache-2.0
URL:           https://metacpan.org/release/List-MoreUtils-XS
Source0:       https://cpan.metacpan.org/authors/id/R/RE/REHSACK/List-MoreUtils-XS-%{version}.tar.gz

BuildRequires: perl-generators perl-interpreter perl(Capture::Tiny) perl(Config::AutoConf) >= 0.315
BuildRequires: coreutils findutils perl(Test::More) >= 0.96 perl(ExtUtils::CBuilder)
BuildRequires: gcc make perl-devel
BuildRequires: perl(base) perl(Exporter) perl(strict) perl(vars) perl(warnings) perl(XSLoader) >= 0.22
BuildRequires: perl(JSON::PP) perl(List::Util) perl(Math::Trig) perl(overload) perl(POSIX) perl(Storable)
BuildRequires: perl(Test::Builder::Module)

%description
List::MoreUtils::XS is a backend for List::MoreUtils. Even if it's possible (because of user wishes)
to have it practically independent from List::MoreUtils, it technically depend on List::MoreUtils.
Since it's only a backend, the API is not public and can change without any warning.

%package_help

%prep
%autosetup -n List-MoreUtils-XS-%{version} -p0

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PERLLOCAL=1 NO_PACKLIST=1 OPTIMIZE="%{optflags}"
%make_build

%install
%make_install
%{_fixperms} -c %{buildroot}

%check
make test

%files
%license ARTISTIC-1.0 GPL-1 LICENSE
%doc Changes README.md
%{perl_vendorarch}/*

%files help
%doc MAINTAINER.md
%{_mandir}/*/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 0.430-3
- drop useless perl(:MODULE_COMPAT) requirement

* Fri Jul 15 2022 misaka00251 <misaka00251@misakanet.cn> - 0.430-2
- Correct BuildRequires in the spec file

* Tue Jan 26 2021 liudabo<liudabo1@huawei.com> - 0.430-1
- upgrade version to 0.430

* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.428-7
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: remove unnecessary files

* Sat Sep 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.428-6
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: remove some notes 

* Tue Sep 10 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.428-5
- Package init
